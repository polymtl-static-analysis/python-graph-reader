# Introduction

Python library to parse `.ast.json` and `.cfg.json` files and load the corresponding graph in memory.\
Additionally, provides methods to walk through, analyse, edit and export the loaded graphs.

# Examples

## Read CFG

```
from code_analysis import CFGReader

reader = CFGReader()
cfg = reader.read_cfg("file.php.cfg.json")

root_node = cfg.get_root()                      # Get CFG root
children  = cfg.get_children(root_node)         # Get children list of node 'root_node'
parents   = cfg.get_parents(root_node)          # Get parents  list of node 'root_node'
```


## Read AST

```
from code_analysis import ASTReader

reader = ASTReader()
ast = reader.read_ast("file.php.ast.json")

root_node = ast.get_root()                      # Get AST root
```
